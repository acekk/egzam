import time
from django.views.decorators.csrf import csrf_exempt
from uuid import uuid4
import AggregateOpts
from rq import Queue
from worker import conn
import json
from CalculatorLIcz import ONP
import operator
from django.http import HttpResponse, Http404
from django.template.response import  TemplateResponse
from django.core.context_processors import csrf
from forms import *
import couchdb
from redis import Redis
import socket
from django.shortcuts import render_to_response


newlist=[]


def hello(request):
    c={}
    c.update(csrf(request))
    for item in operations:
        if item not in newlist:
            newlist.append(item)
        else:
            pass

    return TemplateResponse(request,'simpleTemplate.html',{'blog_entries':"",'information':newlist})

operations=[]
if(len(operations)==0):
    operations=AggregateOpts.AvailibleOpts()
present=False;
server =[]
licznik=0
couch=couchdb.Server('http://194.29.175.241:5984/')
db = couch['calc']
if(len(server)==0):
    for serverz in db:
        licznik=licznik+1
        data = db[serverz]
        try:
            if(data['host']=='http://polar-journey-6461.herokuapp.com/'):
                data['active']=True
                db.save(data)
                present=True
        except:
            pass

        if (present==False):
            doc= {'host':str('http://polar-journey-6461.herokuapp.com/'),'active':True,'operator':'+','calculation':'/SendCalculate/'}
            db.save(doc)
        server.append(serverz)
        if(licznik>15):
            break

def calculate(request):
    c={}
    c.update(csrf(request))
        ##server=db[server]
    hostList = [];
    if request.method=="GET":
        return TemplateResponse(request,'calculate.html',{'form':CalculateForm,'c':c,'information':hostList})

    elif request.method=="POST":
        obliczenie = request.POST.get('obliczenie')

        result = ONP(server,obliczenie,AvlOperations=operations)

        if(result==""):
            return TemplateResponse(request,'error.html', {'information':"Operacje nie moga zostac wykonane (nikt nie udostepnia takich dzialan)"})
        elif(result=="error 404"):
            return TemplateResponse(request,'error.html', {'information':"Wystapil jakis blad na serwerze ktory powinien wykonac nasza operacje"})
        elif(result=="Error"):
            return TemplateResponse(request,'error.html', {'information':"Wystapil jakis blad na serwerze ktory powinien wykonac nasza operacje"})
        else:
            return TemplateResponse(request,'result.html',{'result':result})
        pass

    return TemplateResponse(request,'calculate.html',{'form':CalculateForm,'c':c,'information':server})


